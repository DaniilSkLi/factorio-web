import { MeshType } from "./enums";

export interface Mesh {
    meshType: MeshType,
}
