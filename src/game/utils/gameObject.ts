import Vector from "./vector";

export interface IGameObject {
    position: Vector;
    update: () => void;
}

export class GameObject implements IGameObject {
    public position: Vector = new Vector(0, 0);
    public update(): void {}
}
