export type GameConfig = {
    resolution: { width: number, height: number },
    targetFps: number,
}
