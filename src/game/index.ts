import type { GameConfig } from "./config";
import Render from "./render";
import { GameObject } from "./utils/gameObject";

export default class Game {
    private readonly config: GameConfig;
    private readonly root: HTMLElement;
    private canvas: HTMLCanvasElement;
    private Render: Render;
    private gameObjects: Array<GameObject> = [];

    constructor(config: GameConfig) {
        this.config = config;
        this.root = document.querySelector('#game')!;
        this.canvas = this.createCanvas(this.root);
        this.Render = new Render(this.canvas.getContext('2d')!);
    }

    public start(): void {
        setInterval(() => {
            this.update();
            this.render();
        }, 1000 / this.config.targetFps);
    }

    public addObject(gameObject: GameObject): void {
        this.gameObjects.push(gameObject);
    }

    private update(): void {
        this.gameObjects.forEach((gameObject: GameObject) => {
            gameObject.update();
        });
    }

    private render(): void {
        this.gameObjects.forEach((gameObject: GameObject) => {
            this.Render.object(gameObject);
        });
    }

    private createCanvas(root: HTMLElement): HTMLCanvasElement {
        let canvas: HTMLCanvasElement = document.createElement("canvas");
        canvas.width = this.config.resolution.width;
        canvas.height = this.config.resolution.height;
        canvas.id = "game__canvas";
        root.appendChild(canvas);
        return canvas;
    }
}
