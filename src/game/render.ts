import { GameObject } from "./utils/gameObject";

export default class Render {
    constructor(public ctx: CanvasRenderingContext2D) {}

    public object(gameObject: GameObject) {
        this.ctx.fillRect(gameObject.position.x, gameObject.position.y, 10, 10);
    }
}
