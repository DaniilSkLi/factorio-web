import { GameObject } from "./game/utils/gameObject";

export default class Test extends GameObject {
    public update(): void {
        this.position.y++;
    }
}
