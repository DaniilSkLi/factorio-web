import './style.css'
import Game from "./game";

import {GameObject} from "./game/utils/gameObject";

class Test extends GameObject {
    update(): void {
        this.position.y++;
        this.position.x += 2.5;
    }
}
class Test2 extends GameObject {
    update(): void {
        this.position.y++;
        this.position.x += 2;
    }
}

window.addEventListener("load", () => {
    const game = new Game({
        targetFps: 60,
        resolution: {
            width: 1920,
            height: 1080,
        },
    });
    game.addObject(new Test());
    game.addObject(new Test2());
    game.start();
});
