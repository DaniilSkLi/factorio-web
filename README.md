# Factorio Web

## Original
Factorio is a game about building a factory. 
The original site of the game [www.factorio.com](https://www.factorio.com/).  
I'm not its author, I did not come up with the idea and name, I just like this game and I wanted to try as an experiment to make it as a web version.

## License

This project is licensed under the GNU General Public License v3.0.  
A copy of the license is available in the file [LICENSE](LICENSE).

The authors of the project, their contact information and their contributions are listed in the file [AUTHORS](AUTHORS)
